INSERT INTO LECC.TASK_CORRIDORS_SEGMENT
(TASK_ID, SEGMENT_ID, CORRIDOR_ID, PLAN_HOURS)
SELECT TASK_ID, SEGMENT_ID, CORRIDOR_ID, PLAN_HOURS FROM
(
SELECT 
 ts.ID  as TASK_ID
	,s.ID AS SEGMENT_ID 
	,cr.id AS CORRIDOR_ID
	,4 AS PLAN_HOURS 
FROM 
	tasks ts,
	SEGMENTS s,
	CORRIDORS cr
WHERE SUBSTR(ts.code,0,3) <> 'R2-'  
AND SUBSTR(ts.code,INSTR(ts.code,'-',1,1)+1,LENGTH(ts.code))   IN (
'1CR-T1'
,'1LCR-T1'
,'1LCR-T2'
,'1NR-T1'
,'2.1-T2'
)
AND s.code IN (
'MidCap',
'Tier 1.2',
'Tier 1.1',
'Tier 2'
)
UNION 
SELECT 
	ts.ID  as TASK_ID
	,s.ID AS SEGMENT_ID 
	,cr.id AS CORRIDOR_ID
	,CASE
		WHEN  s.CODE  = 'MidCap' AND cr.CODE <> 'RED' THEN 7
		WHEN  s.CODE <> 'MidCap' AND cr.CODE = 'GREEN' THEN 4
		WHEN  s.CODE <> 'MidCap' AND cr.CODE = 'YELLOW' THEN 6
		ELSE 10
	END  AS PLAN_HOURS 	
FROM 
	tasks ts,
	SEGMENTS s,
	CORRIDORS cr
WHERE SUBSTR(ts.code,0,3) <> 'R2-'  
AND SUBSTR(ts.code,INSTR(ts.code,'-',1,1)+1,LENGTH(ts.code))  IN (
'2.1-T5'
,'2.1-T7'
)
AND s.code IN (
'MidCap',
'Tier 1.2',
'Tier 1.1',
'Tier 2'
)
UNION 
SELECT 
	ts.ID  as TASK_ID
	,s.ID AS SEGMENT_ID 
	,cr.id AS CORRIDOR_ID
	,4 AS PLAN_HOURS 
FROM 
	tasks ts,
	SEGMENTS s,
	CORRIDORS cr
WHERE SUBSTR(ts.code,0,3) <> 'R2-'  
AND SUBSTR(ts.code,INSTR(ts.code,'-',1,1)+1,LENGTH(ts.code))  IN (
'2.3-T0'
,'2.3-T1'
)
AND s.code IN (
'MidCap'
)
AND cr.CODE IN(
'GREEN'
,'YELLOW'
)
UNION 
SELECT 
	ts.ID  as TASK_ID
	,s.ID AS SEGMENT_ID 
	,cr.id AS CORRIDOR_ID
	,4 AS PLAN_HOURS 
FROM 
	tasks ts,
	SEGMENTS s,
	CORRIDORS cr
WHERE SUBSTR(ts.code,0,3) <> 'R2-'  
AND SUBSTR(ts.code,INSTR(ts.code,'-',1,1)+1,LENGTH(ts.code))  IN (
'2.3-T0'
,'2.3-T1'
)
AND s.code IN (
'Tier 1.2',
'Tier 1.1',
'Tier 2'
)
AND cr.CODE IN(
'GREEN'
)
UNION 
SELECT 
	ts.ID  as TASK_ID
	,s.ID AS SEGMENT_ID 
	,cr.id AS CORRIDOR_ID
	,4 AS PLAN_HOURS 
FROM 
	tasks ts,
	SEGMENTS s,
	CORRIDORS cr
WHERE SUBSTR(ts.code,0,3) <> 'R2-'  
AND SUBSTR(ts.code,INSTR(ts.code,'-',1,1)+1,LENGTH(ts.code))  IN (
'2.3-T1.1'
,'2.3-T1.2'
,'2.3-T2'
,'2.3-T2.1'
,'2.3-T3'
,'2.3-T3.1'
,'2.3-T4'
,'2.3-T4.1'
,'2.3-T5'
,'2.3-T5.1'
,'2.3-T6'
,'2.3-T6.1'
,'2.3-T7'
,'2.3-T7.1'
,'2.3-T7.2'
,'2.3-T7.3'
)
AND s.code IN (
'MidCap',
'Tier 1.2',
'Tier 1.1',
'Tier 2'
)
AND cr.CODE IN(
'GREEN'
)
UNION 
SELECT 
	ts.ID  as TASK_ID
	,s.ID AS SEGMENT_ID 
	,cr.id AS CORRIDOR_ID
	,CASE WHEN cr.CODE = 'RED' THEN 4 ELSE 3 END AS PLAN_HOURS 
FROM 
	tasks ts,
	SEGMENTS s,
	CORRIDORS cr
WHERE SUBSTR(ts.code,0,3) <> 'R2-'  
AND SUBSTR(ts.code,0,2) <> 'R4' 
AND SUBSTR(ts.code,INSTR(ts.code,'-',1,1)+1,LENGTH(ts.code))  IN (
'3.1-T2'
)
AND s.code IN (
'MidCap'
)
AND cr.CODE IN(
'YELLOW'
,'RED'
)
UNION 
SELECT 
	ts.ID  as TASK_ID
	,s.ID AS SEGMENT_ID 
	,cr.id AS CORRIDOR_ID
	,12 AS PLAN_HOURS 
FROM 
	tasks ts,
	SEGMENTS s,
	CORRIDORS cr
WHERE SUBSTR(ts.code,0,3) <> 'R2-'  
AND SUBSTR(ts.code,0,2) <> 'R4' 
AND SUBSTR(ts.code,INSTR(ts.code,'-',1,1)+1,LENGTH(ts.code))  IN (
'3.1-T2'
)
AND s.code IN (
'Tier 1.2',
'Tier 1.1',
'Tier 2'
)
AND cr.CODE IN(
'GREEN'
)
UNION 
SELECT 
	ts.ID  as TASK_ID
	,s.ID AS SEGMENT_ID 
	,cr.id AS CORRIDOR_ID
	,CASE WHEN cr.CODE = 'RED' THEN 8 ELSE 4 END AS PLAN_HOURS 
FROM 
	tasks ts,
	SEGMENTS s,
	CORRIDORS cr
WHERE SUBSTR(ts.code,0,3) <> 'R2-'  
AND SUBSTR(ts.code,0,2) <> 'R4' 
AND SUBSTR(ts.code,INSTR(ts.code,'-',1,1)+1,LENGTH(ts.code))  IN (
'3.1-T3'
)
AND s.code IN (
'MidCap',
'Tier 1.2',
'Tier 1.1',
'Tier 2'
)
AND cr.CODE IN(
'GREEN',
'RED'
)
)

