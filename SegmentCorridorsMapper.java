package sl.lecc.services.mappers;

import sl.lecc.services.model.IntervalCorridor;
import sl.lecc.services.model.TaskCorridor;
import sl.lecc.services.model.TaskSegmentCorridor;

import java.util.List;
import java.util.Map;

public interface SegmentCorridorsMapper {

    List<TaskSegmentCorridor> getAllCorridorByTaskAndSegment(Map<String, Object> params);

}
