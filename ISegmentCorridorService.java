package sl.lecc.services;

import sl.lecc.services.model.TaskSegmentCorridor;

import java.util.List;

public interface ISegmentCorridorService {

    List<TaskSegmentCorridor> getAllCorridorByTaskAndSegment(long taskId, long segmentId);
}
