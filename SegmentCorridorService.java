package sl.lecc.services;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import sl.lecc.services.mappers.SegmentCorridorsMapper;
import sl.lecc.services.model.TaskSegmentCorridor;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Stateless
@Local(ISegmentCorridorService.class)
public class SegmentCorridorService implements ISegmentCorridorService{

    private static final Logger LOG = Logger.getLogger(SegmentCorridorService.class.getName());

    @Resource(name = "lecc/core")
    private DataSource dataSource;

    private SqlSessionFactory sqlSessionFactory;

    @PostConstruct
    @TransactionAttribute(javax.ejb.TransactionAttributeType.REQUIRED)
    public void initialize() {
        SessionFactoryHelper sessionFactoryHelper = new SessionFactoryHelper();
        sqlSessionFactory = sessionFactoryHelper.getSqlSessionFactory(dataSource);

        LOG.info("SegmentCorridorService initialized");
    }


    @Override
    public List<TaskSegmentCorridor> getAllCorridorByTaskAndSegment(long taskId, long segmentId) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()){
            Map<String,Object> params = new HashMap<>();
            params.put("taskId",taskId);
            params.put("segmentId",segmentId);
            return sqlSession.getMapper(SegmentCorridorsMapper.class).getAllCorridorByTaskAndSegment(params);
        }
    }
}
