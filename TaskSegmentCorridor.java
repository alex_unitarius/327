package sl.lecc.services.model;

import java.io.Serializable;

public class TaskSegmentCorridor implements Serializable {
    private Long id;
    private Long taskId;
    private Long segmentId;
    private Long corridorId;
    private Integer planHours;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(Long segmentId) {
        this.segmentId = segmentId;
    }

    public Long getCorridorId() {
        return corridorId;
    }

    public void setCorridorId(Long corridorId) {
        this.corridorId = corridorId;
    }

    public Integer getPlanHours() {
        return planHours;
    }

    public void setPlanHours(Integer planHours) {
        this.planHours = planHours;
    }
}
